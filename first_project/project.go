package main

import "fmt"

func main(){
  var a = 29
  var b = 1
  var r int
  r = summ(a, b)
  fmt.Println(r)

  var arr[2] int = makeArray(a, b)

  for i := 0; i < 2; i++{
    fmt.Println(arr[i])
  }

  var num = 3

  multiple := func() int {
    num *= 2
    return num
  }

  fmt.Println(multiple())

  //defer two()
  //one()

  var x = 0
  pointer(&x)
  fmt.Println(x)

  bob := Cats{"bob", 7, 0.87}
  fmt.Println("Bob age is", bob.age)
  fmt.Println("Bob function is", bob.test())

}

type Cats struct {
  name string
  age int
  happiness float64
}

func (cat *Cats) test() float64{
  return float64(cat.age) * cat.happiness
}

func summ(num_1 int, num_2 int) int{
  var res int
  res = num_1 + num_2
  return res
}

func makeArray(num_1 int, num_2 int) [2]int{
  nums := [2]int {num_1, num_2}
  return nums
}

func one() {
  fmt.Println("one")
}

func two() {
  fmt.Println("two")
}

func pointer(x *int){
  *x = 2
}
